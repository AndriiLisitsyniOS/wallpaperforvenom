//
//  FullScreenPicsCell.swift
//  wallpaperForVenum
//
//  Created by andrey on 8/17/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit

class FullscreenPicsCell: UICollectionViewCell {
    
    @IBOutlet weak var fullscreenImgView: UIImageView!
    
}
