//
//  FullscreenPicsVC.swift
//  wallpaperForVenum
//
//  Created by andrey on 8/17/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit

class FullscreenPicsVC: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, PurchaseManagerDelegate {
  
    @IBOutlet weak var fullscreenPicsCollectionView: UICollectionView!
    @IBOutlet weak var filtersBtn: UIButton!
    @IBOutlet weak var filtersScroll: UIScrollView!
    @IBOutlet weak var filtersView: UIView!
    @IBOutlet weak var backBtn: UIButton!
    @IBOutlet weak var previewBtn: UIButton!
    @IBOutlet weak var noAdsBtn: UIButton!
    @IBOutlet weak var previewTopLbl: UILabel!
    @IBOutlet weak var previewView: UIView!
    @IBOutlet weak var previewBottomLbl: UILabel!
    @IBOutlet weak var activityVCBtn: UIButton!
    @IBOutlet weak var activityVCView: UIView!
    @IBOutlet weak var removeAdsView: UIView!
    @IBOutlet weak var removeAdsBtnsStack: UIStackView!
    @IBOutlet weak var iPadNavView: UIView!
    
    var picsArray = [UIImage]()
    var smallPicsArray = [UIImage]()
    var lightImgsNumArray = [Int]()
    var blackPreviewImgsNumArray = [Int]()
    var currentPage = 0
    var previousPage = 0
    var filterBtnsArray = [UIButton]()
    var activityIndicatorsArray = [UIActivityIndicatorView]()
    var iPhone = false
    var filteredImg = UIImage()
    var filtering = false
    var activityVCBottomCN = NSLayoutConstraint()
    var filtersMode = false
    var filterFirstPress = false
    var previewMode = false
    var showHideBtns = true
    var navBtnsArray = [UIButton]()
    var navImgsArray = [UIImage]()
    var firstAppear = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        PurchaseManager.instance.delegate = self
        fullscreenPicsCollectionView.delegate = self
        fullscreenPicsCollectionView.dataSource = self
        lightImgsNumArray = Pics.instance.getLightImgs()
        blackPreviewImgsNumArray = Pics.instance.getBlackPreviewImgs()
        switch UIDevice.current.userInterfaceIdiom {
            case .phone:
                iPhone = true
            default:
                iPhone = false
        }
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap(_:)))
        view.addGestureRecognizer(tap)
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToDownSwipeGesture))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        previousPage = currentPage
        
        guard iPhone else { return }
        var lightBg = false
        for l in self.lightImgsNumArray {
            if l == self.currentPage {
                lightBg = true
                break
            }
        }
        navImgsArray = Pics.instance.getNavImgs()
        navBtnsArray = [noAdsBtn, filtersBtn, previewBtn, activityVCBtn, backBtn]
        for x in 0...navBtnsArray.count - 1 {
            let tintedImg = navImgsArray[x].withRenderingMode(.alwaysTemplate)
            navBtnsArray[x].setImage(tintedImg, for: .normal)
            if lightBg {
                navBtnsArray[x].tintColor = #colorLiteral(red: 0.1843137255, green: 0.2352941176, blue: 0.2666666667, alpha: 1)
            } else {
                navBtnsArray[x].tintColor = .white
            }
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        guard firstAppear else { return }
        GoogleAds.instance.page += 1
        if GoogleAds.instance.page % 3 == 0 {
            GoogleAds.instance.appearInterstitial(vc: self)
            GoogleAds.instance.createAndLoadInterstitial()
        }
        firstAppear = false
    }

    override func viewDidLayoutSubviews() {
        fullscreenPicsCollectionView.setContentOffset(CGPoint(x: view.bounds.width * CGFloat(currentPage), y: 0), animated: false)
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if previewMode {
            previewTopLbl.isHidden = true
            previewBottomLbl.isHidden = true
            showBtns()
            if !filtersMode {
                fullscreenPicsCollectionView.isScrollEnabled = true
            }
            if !previewView.isHidden {
                previewView.isHidden = true
            }
            previewMode = false
        } else {
            if showHideBtns {
                hideBtns()
            } else {
                showBtns()
            }
            showHideBtns = !showHideBtns
        }
    }
    
    @objc func respondToDownSwipeGesture(gesture: UIGestureRecognizer) {
        dismiss(animated: true, completion: nil)
    }
    
    func showBtns() {
        backBtn.isHidden = false
        activityVCBtn.isHidden = false
        previewBtn.isHidden = false
        noAdsBtn.isHidden = false
        filtersBtn.isHidden = false
        if filtersMode {
            filtersScroll.isHidden = false
        }
        guard iPhone == false else { return }
        iPadNavView.isHidden = false
    }
    
    func hideBtns() {
        backBtn.isHidden = true
        activityVCBtn.isHidden = true
        previewBtn.isHidden = true
        noAdsBtn.isHidden = true
        filtersBtn.isHidden = true
        if filtersMode {
            filtersScroll.isHidden = true
        }
        guard iPhone == false else { return }
        iPadNavView.isHidden = true
    }

    @IBAction func filtersBtnPressed(_ sender: Any) {
        if filtersMode == false {
            createFilterBtns()
            filterFirstPress = true
            filtersMode = true
            filteredImg = UIImage()
            if iPhone {
                filtersBtn.setImage(UIImage(named: "x")!.withRenderingMode(.alwaysTemplate), for: .normal)
            } else {
                filtersBtn.setImage(UIImage(named: "x"), for: .normal)
            }
            filtersScroll.isHidden = false
            fullscreenPicsCollectionView.isScrollEnabled = false
            filterBtnsArray[0].layer.borderWidth = 3
            filterBtnsArray[0].layer.borderColor = UIColor.red.cgColor
            let q1 = OperationQueue()
            for f in 0...8 {
                activityIndicatorsArray[f].startAnimating()
                activityIndicatorsArray[f].isHidden = false
                q1.addOperation {
                    let filterIcon = Filters.instance.filterImg(filterNum: f, image: self.smallPicsArray[self.currentPage])
                    DispatchQueue.main.async() {
                        if f == 0 {
                            self.filterBtnsArray[0].setImage(self.smallPicsArray[self.currentPage], for: .normal)
                        } else {
                            self.filterBtnsArray[f].setImage(filterIcon, for: .normal)
                        }
                        self.activityIndicatorsArray[f].stopAnimating()
                        self.activityIndicatorsArray[f].isHidden = true
                    }
                }
            }
        } else {
            closeFilters()
        }
    }
    
    func createFilterBtns() {
        guard filterFirstPress == false else { return }
        for f in 0...8 {
            let btn = UIButton(type: .custom)
            if view.bounds.width >= 752 {
                btn.frame = CGRect(x: (view.bounds.width - 752)/2 + 5 + CGFloat(f)*83, y: 10, width: 78, height: 78)
                filtersScroll.isScrollEnabled = false
            } else {
                btn.frame = CGRect(x: 5 + CGFloat(f)*83, y: 10, width: 78, height: 78)
            }
            btn.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
            btn.imageView?.contentMode = UIViewContentMode.scaleAspectFill
            btn.tag = f
            btn.addTarget(self, action: #selector(filterBtnAction(sender:)), for: .touchUpInside)
            let activityIndicator = UIActivityIndicatorView()
            activityIndicator.frame = CGRect(x: 31.5, y: 31.5, width: 20, height: 20)
            activityIndicatorsArray.append(activityIndicator)
            btn.addSubview(activityIndicator)
            filterBtnsArray.append(btn)
            filtersView.addSubview(btn)
        }
    }
    
    func closeFilters() {
        filtersMode = false
        filtersScroll.isHidden = true
        if iPhone {
            filtersBtn.setImage(UIImage(named: "filters")!.withRenderingMode(.alwaysTemplate), for: .normal)
        } else {
            filtersBtn.setImage(UIImage(named: "filters"), for: .normal)
        }
        
        for f in filterBtnsArray {
            f.layer.borderColor = UIColor.clear.cgColor
            f.setImage(nil, for: .normal)
        }
        filtersScroll.setContentOffset(CGPoint(x: 0, y: 0), animated: false)
        fullscreenPicsCollectionView.isScrollEnabled = true
        fullscreenPicsCollectionView.reloadData()
    }
    
    @objc func filterBtnAction(sender: UIButton) {
        guard sender.layer.borderColor != UIColor.red.cgColor else { return }
        for f in filterBtnsArray {
            f.layer.borderColor = UIColor.clear.cgColor
        }
        sender.layer.borderWidth = 3
        sender.layer.borderColor = UIColor.red.cgColor
        filtering = true
        if sender.tag == 0 {
            filteredImg = picsArray[currentPage]
        } else {
            filteredImg = Filters.instance.filterImg(filterNum: sender.tag, image: picsArray[currentPage])
        }
        fullscreenPicsCollectionView.reloadData()
    }
    
    @IBAction func previewBtnPressed(_ sender: Any) {
        var blackPreview = false
        for n in blackPreviewImgsNumArray {
            if n == currentPage {
                blackPreview = true
            }
        }
//        let image = smallPicsArray[currentPage]
//        if image.areaAverage().isLight
        if blackPreview {
            previewTopLbl.textColor = #colorLiteral(red: 0.1843137255, green: 0.2352941176, blue: 0.2666666667, alpha: 1)
            previewBottomLbl.textColor = #colorLiteral(red: 0.1843137255, green: 0.2352941176, blue: 0.2666666667, alpha: 1)
        } else {
            previewView.isHidden = false
            previewTopLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            previewBottomLbl.textColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        previewMode = true
        if iPhone {
            previewTopLbl.font = previewTopLbl.font.withSize(view.bounds.width/4.7)
            previewBottomLbl.font = previewBottomLbl.font.withSize(view.bounds.width/17.9)
        } else {
            previewTopLbl.font = previewTopLbl.font.withSize(view.bounds.width/7.2)
            previewBottomLbl.font = previewBottomLbl.font.withSize(view.bounds.width/33.4)
        }
        hideBtns()
        previewTopLbl.isHidden = false
        previewBottomLbl.isHidden = false
        fullscreenPicsCollectionView.isScrollEnabled = false
    }
    
    @IBAction func activityVCBtnPressed(_ sender: Any) {
        var img = UIImage()
        if filtersMode && filterBtnsArray[0].layer.borderColor != UIColor.red.cgColor  {
            img = filteredImg
        } else {
            img = picsArray[currentPage]
        }
        let text = "Top wallpaper for Venom are here!\nhttps://itunes.apple.com/app/id1435604625"
        let activityItems = [ img, text ] as [Any]
        let activityVC = UIActivityViewController(activityItems: activityItems, applicationActivities: nil)
        activityVC.completionWithItemsHandler = { activity, success, items, error in
            if activity == .saveToCameraRoll {
                if #available(iOS 10.3, *){
                    let appDelegate = AppDelegate()
                    appDelegate.requestReview()
                } else if UserDefaults.standard.bool(forKey: "rated") != true {
                    self.showAskReviewAlert()
                }
            }
        }

        present(activityVC, animated: true, completion: nil)
        if let popOver = activityVC.popoverPresentationController {
            popOver.sourceView = activityVCView
            popOver.sourceRect = CGRect(x: 0, y: 0, width: 200, height: 200)
        }
    }
    
    func showAskReviewAlert() {
        let alert = UIAlertController(title: "Rate Us\n", message:"If you enjoy this app, \nplease support us.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Rate App", style: .cancel) { _ in
            UIApplication.shared.open(URL(string : "itms-apps://itunes.apple.com/app/id1435604625")!, options: [:], completionHandler: nil)
            UserDefaults.standard.set(true, forKey: "rated")
        })
        alert.addAction(UIAlertAction(title: "Not now", style: .default) { _ in
        })
        self.present(alert, animated: true){}
    }
    
    @IBAction func noAdsBtnPressed(_ sender: Any) {
        removeAdsView.isHidden = false
        removeAdsBtnsStack.isHidden = false
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.handleNoAdViewTap(_:)))
        removeAdsView.addGestureRecognizer(tap)
    }
    
    @objc func handleNoAdViewTap(_ sender: UITapGestureRecognizer) {
        removeAdsView.isHidden = true
        removeAdsBtnsStack.isHidden = true
    }
    
    @IBAction func buyNoAdsPressed(_ sender: Any) {
        PurchaseManager.instance.selectProduct(productNum: 1)
    }
    @IBAction func restoreBtnPressed(_ sender: Any) {
        PurchaseManager.instance.restorePurchases()
    }
    
    func closeMenu() {
        removeAdsView.isHidden = true
        removeAdsBtnsStack.isHidden = true
    }

    @IBAction func backBtnPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: UICollectionViewDelegate, UICollectionViewDataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Pics.instance.getPicsNumber()
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = fullscreenPicsCollectionView.dequeueReusableCell(withReuseIdentifier: "fullscreenPicsCell", for: indexPath) as! FullscreenPicsCell
        cell.fullscreenImgView.image = picsArray[indexPath.section]
        if filtering {
            if indexPath.section == currentPage {
                cell.fullscreenImgView.image = filteredImg
                filtering = false
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width, height: view.bounds.height)
    }
    
    // MARK: UISrollViewDelegate
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let x = fullscreenPicsCollectionView.contentOffset.x
        let w = fullscreenPicsCollectionView.bounds.size.width
        currentPage = Int(ceil(x/w))
        guard currentPage != previousPage else { return }
        previousPage = currentPage
        GoogleAds.instance.page += 1
        if GoogleAds.instance.page % 3 == 0 {
            GoogleAds.instance.appearInterstitial(vc: self)
            GoogleAds.instance.createAndLoadInterstitial()
        }
        guard iPhone else { return }
        var lightBg = false
        for l in self.lightImgsNumArray {
            if l == self.currentPage {
                lightBg = true
                break
            }
        }
        UIView.animate(withDuration: 0.25, animations: {
            for b in self.navBtnsArray {
                if lightBg {
                    b.tintColor = #colorLiteral(red: 0.1843137255, green: 0.2352941176, blue: 0.2666666667, alpha: 1)
                } else {
                    b.tintColor = .white
                }
            }
        })
    }
}

