//
//  ViewController.swift
//  wallpaperForVenum
//
//  Created by andrey on 8/17/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var picsCollectionView: UICollectionView!
    @IBOutlet weak var launchImgView: UIImageView!
    var smallPicsArray = [UIImage]()
    var picsArray = [UIImage]()
    var picsNum = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(hideLaunchImg), userInfo: nil, repeats: true)
        smallPicsArray = Pics.instance.getSmallPicsArray()
        picsArray = Pics.instance.getPicsArray()
        picsNum = Pics.instance.getPicsNumber()
        
        picsCollectionView.delegate = self
        picsCollectionView.dataSource = self
        if #available(iOS 11.0, *) {
            picsCollectionView.contentInsetAdjustmentBehavior = .never
        }
    }
    
    @objc func hideLaunchImg() {
        UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseOut,
                       animations: {self.launchImgView.alpha = 0},
                       completion: { _ in self.launchImgView.isHidden = true
        })
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if picsNum % 3 == 0 {
            return picsNum/3
        } else {
            return picsNum/3 + 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == picsNum/3  {
            return picsNum % 3
        } else {
            return 3
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = picsCollectionView.dequeueReusableCell(withReuseIdentifier: "picsCell", for: indexPath) as! PicsCell
        let q1 = OperationQueue()
        q1.addOperation {
            let image = self.smallPicsArray[indexPath.section*3 + indexPath.row]
            DispatchQueue.main.async() {
                cell.imgView.image = image
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.bounds.width/3, height: view.bounds.height/3)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "showCell", sender: indexPath.section*3 + indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showCell" {
            let fullscreenPicsVC = segue.destination as? FullscreenPicsVC
            fullscreenPicsVC?.currentPage = sender as! Int
            fullscreenPicsVC?.picsArray = picsArray
            fullscreenPicsVC?.smallPicsArray = smallPicsArray
        }
    }

}

