//
//  Pics.swift
//  wallpaperForVenum
//
//  Created by andrey on 8/17/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation
import UIKit

class Pics {
    
    static let instance = Pics()
    var picsArray = [UIImage]()
    var smallPicsArray = [UIImage]()
    
    func getPicsNumber() -> Int {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            return 44
        default:
            return 44
        }
    }
    
    func getPicsArray() -> [UIImage] {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            if UIScreen.main.nativeBounds.height == 2436 {
                for p in 1...getPicsNumber() {
                    picsArray.append(UIImage(named: "\(p)x.jpg")!)
                }
            } else {
                for p in 1...getPicsNumber() {
                    picsArray.append(UIImage(named: "\(p).jpg")!)
                }
            }
        default:
            for p in 1...getPicsNumber() {
                picsArray.append(UIImage(named: "\(p)iPad.jpg")!)
            }
        }
        return picsArray
    }
    
    func getSmallPicsArray() -> [UIImage] {
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            if UIScreen.main.nativeBounds.height == 2436 {
                for p in 1...getPicsNumber() {
                    smallPicsArray.append(UIImage(named: "s\(p)x.jpg")!)
                }
            } else {
                for p in 1...getPicsNumber() {
                    smallPicsArray.append(UIImage(named: "s\(p).jpg")!)
                }
            }
        default:
            for p in 1...getPicsNumber() {
                smallPicsArray.append(UIImage(named: "s\(p)iPad.jpg")!)
            }
        }
        return smallPicsArray
    }
    
    func getLightImgs() -> [Int] {
        var lightImgsNumArray = [Int]()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            if UIScreen.main.nativeBounds.height == 2436 {
                lightImgsNumArray = [0, 7, 11, 27, 37]
            } else {
                lightImgsNumArray = [0, 7, 13, 27, 35]
            }
        default:
            lightImgsNumArray = [3, 9, 21, 30, 38, 43]
        }
        return lightImgsNumArray
    }
    
    func getBlackPreviewImgs() -> [Int] {
        var blackPreviewImgsNumArray = [Int]()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            if UIScreen.main.nativeBounds.height == 2436 {
                blackPreviewImgsNumArray = [0, 11, 27, 37]
            } else {
                blackPreviewImgsNumArray = [0, 27, 35]
            }
        default:
            blackPreviewImgsNumArray = [9, 21, 38, 43]
        }
        return blackPreviewImgsNumArray
    }
    
    func getNavImgs() -> [UIImage] {
        var navImgsArray = [UIImage]()
        switch UIDevice.current.userInterfaceIdiom {
        case .phone:
            navImgsArray = [UIImage(named: "noAd"), UIImage(named: "filters"), UIImage(named: "preview"), UIImage(named: "activityVC"), UIImage(named: "down")] as! [UIImage]
        default:
            navImgsArray = [UIImage(named: "noAd"), UIImage(named: "filters"), UIImage(named: "preview"), UIImage(named: "activityVC"), UIImage(named: "x")] as! [UIImage]
        }
        return navImgsArray
    }
}
