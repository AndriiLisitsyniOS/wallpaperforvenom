//
//  PurchaseManager.swift
//  wallpaperForVenom
//
//  Created by andrey on 9/7/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation
import StoreKit

class PurchaseManager: NSObject, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    static let instance = PurchaseManager()
    
    let IAP_removeAds = "com.waygem.noAds"
    var products = [SKProduct]()
    var product = SKProduct()
    var delegate: PurchaseManagerDelegate?
    
    func fetchProducts() {
        if(SKPaymentQueue.canMakePayments()) {
            let productIds = NSSet(object: IAP_removeAds) as! Set<String>
            let productsRequest: SKProductsRequest = SKProductsRequest(productIdentifiers: productIds)
            productsRequest.delegate = self
            productsRequest.start()
        }
    }
    
    func buyProduct() {
        let pay = SKPayment(product: product)
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(pay as SKPayment)
    }
    
    func selectProduct(productNum: Int) {
        var productID = ""
        switch productNum {
        case 1:
            productID = IAP_removeAds
        default:
            productID = IAP_removeAds
        }
        for p in products {
            if p.productIdentifier == productID {
                product = p
                buyProduct()
            }
        }
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        let myProducts = response.products
        for product in myProducts {
            products.append(product)
        }
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                let productID = product.productIdentifier
                switch productID {
                case IAP_removeAds:
                    UserDefaults.standard.set(true, forKey: "noAds")
                    delegate?.closeMenu()
                default:
                    print("IAP not found")
                }
                queue.finishTransaction(transaction)
            case .failed:
                queue.finishTransaction(transaction)
                break
            default:
                break
            }
        }
    }
    
    func paymentQueueRestoreCompletedTransactionsFinished(_ queue: SKPaymentQueue) {
        print("transactions restored")
        for transaction in queue.transactions {
            let productID = transaction.payment.productIdentifier as String
            switch productID {
            case IAP_removeAds:
                UserDefaults.standard.set(true, forKey: "noAds")
                delegate?.closeMenu()
                print("restored")
            default:
                print("IAP not found")
            }
        }
    }
    
    func restorePurchases() {
        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().restoreCompletedTransactions()
    }
}

protocol PurchaseManagerDelegate {
    func closeMenu()
}
