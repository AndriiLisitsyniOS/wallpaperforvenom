//
//  GoogleAds.swift
//  wallpaperForVenom
//
//  Created by andrey on 9/7/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation
import GoogleMobileAds

class GoogleAds {
    static let instance = GoogleAds()
    
    var interstitial: GADInterstitial!
    var page = 0
    
    
    func createAndLoadInterstitial() {
        guard UserDefaults.standard.bool(forKey: "noAds") == false else { return }
        interstitial = GADInterstitial(adUnitID: "ca-app-pub-2220353646614349/2564275082")
        let request = GADRequest()
        request.testDevices = ["b5b36ccd53370e45f0f66a6aa0e0c447", "c2fc9ae806b3e1b2a539dbffb5b85f4b"]
        interstitial.load(request)
    }
    
    func appearInterstitial(vc: UIViewController) {
        guard UserDefaults.standard.bool(forKey: "noAds") == false else { return }
        if interstitial.isReady {
            interstitial.delegate = vc as? GADInterstitialDelegate
            interstitial.present(fromRootViewController: vc)
        } else {
            print("Ad wasn't ready")
        }
    }
}
