//
//  Filters.swift
//  wallpaperForVenum
//
//  Created by andrey on 8/20/18.
//  Copyright © 2018 waygem. All rights reserved.
//

import Foundation
import UIKit

class Filters {
    
    static let instance = Filters()
    
    func filterImg(filterNum: Int, image: UIImage) -> UIImage {
        var filteredImg = UIImage()
        let cgImage = image.cgImage
        let filter = CIFilter(name:"\(selectFilter(number: filterNum))")!
        let openGLContext = EAGLContext(api: .openGLES2)
        let context = CIContext(eaglContext: openGLContext!)
        let ciImage = CIImage(cgImage: cgImage!)
        filter.setValue(ciImage, forKey: kCIInputImageKey)
        if let output = filter.value(forKey: kCIOutputImageKey) as? CIImage {
            filteredImg = UIImage(cgImage: context.createCGImage(output, from: output.extent)!)
        }
        return filteredImg
    }
    
    func selectFilter(number: Int) -> String {
        switch number{
            case 1:
                return "CIPhotoEffectTransfer"
            case 2:
                return "CIPhotoEffectChrome"
            case 3:
                return "CIPhotoEffectProcess"
            case 4:
                return "CISRGBToneCurveToLinear"
            case 5:
                return "CIColorPosterize"
            case 6:
                return "CIPhotoEffectNoir"
            case 7:
                return "CIPhotoEffectFade"
            case 8:
                return "CISepiaTone"
            default:
                return "CIPhotoEffectTransfer"
        }
    }
    
}
